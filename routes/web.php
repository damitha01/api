<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api/v1'], function () use ($router) {
    // User Login
    $router->post('/login', 'UserController@login');

    // Create Customer
    $router->post('/create_customer', 'CustomerController@create');

    // Update Customer
    $router->post('/update_customer', 'CustomerController@update');

    // Gets All Customers
    $router->get('/customer_list', 'CustomerController@index');

    // Search Customers related to user input word
    $router->get('/search_customers/{term}', 'CustomerController@searchCustomers');

    // Delete Customer 
    $router->delete('/customer/{id}', 'CustomerController@deleteCustomer');

    // Gets Customer related details for given customer id
    $router->get('/customer/{id}', 'CustomerController@getCustomerbyId');

    // Create or Update customers using Uploaded CSV file as a bulk
    $router->post('/upload_csv', 'CustomerController@uploadCsv');

});