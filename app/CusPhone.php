<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CusPhone extends Model
{
    protected $table = 'cus_phones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'phone'
    ];

}