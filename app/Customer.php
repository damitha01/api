<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','address1','address2','city','email'
    ];

    
    public function phones()
    {
        return $this->hasMany('App\CusPhone', 'customer_id', 'id');
    }


}