<?php

namespace App\Http\Controllers;

use Log;
use App\Customer;
use App\CusPhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $customer = Customer::with('phones')->orderBy('first_name')->get();
    return response()->json($customer);
  }

  /**
   * Store a new Customer.
   *
   * @param  Request  $request
   * @return Response
   */
  public function create(Request $request)
  {
    //validate incoming request 
    $this->validate($request, [
        'first_name' => 'required|string',
        'last_name'  => 'required|string',
        'email'      => 'required|email',
    ]);

    try 
    {
      $customer = new Customer;
      $customer->first_name = $request->input('first_name');
      $customer->last_name = $request->input('last_name');
      $customer->address1 = $request->input('address1');
      $customer->address2 = $request->input('address2');
      $customer->city = $request->input('city');
      $customer->email = $request->input('email');
      $customer->save();

      $phonecount = $request->input('phonecount');
      for($i = 0; $i < $phonecount; $i++) {
        if (!empty($request->input('phone_' . $i)))
        {
          $cusPhone = new CusPhone;
          $cusPhone->customer_id = $customer->id;
          $cusPhone->phone = $request->input('phone_' . $i);
          $cusPhone->save();
        }
        
      }

      Log::info('Customer Created: Customer Id: '.$customer->id);
      //return successful response
      return response()->json(['res' => 'Customer created successfully.!', 'status' => 1]);

    } catch (\Exception $e) {
        //return error message
        Log::info('Customer Registration Error');
        return response()->json(['res' => 'Customer Registration Failed!', 'status' => 0]);
    }
  }

  /**
   * Update customer data.
   *
   * @param  Request  $request
   * @return Response
   */
  public function update(Request $request)
  {
    //validate incoming request 
    $this->validate($request, [
        'first_name' => 'required|string',
        'last_name'  => 'required|string',
        'email'      => 'required|email|unique:users',
    ]);

    try 
    {
      $customerId = $request->input('customer_id');
      $customer = Customer::find($customerId);
      $customer->first_name = $request->input('first_name');
      $customer->last_name = $request->input('last_name');
      $customer->address1 = $request->input('address1');
      $customer->address2 = $request->input('address2');
      $customer->city = $request->input('city');
      $customer->email = $request->input('email');
      $customer->update();

      $cusPhone = CusPhone::where(['customer_id' => $customerId])->get();
      if ($cusPhone)
        foreach($cusPhone AS $phone)
          $phone->delete();

      $phonecount = $request->input('phonecount');
      for($i = 0; $i < $phonecount; $i++) {
        if (!empty($request->input('phone_' . $i)))
        {
          $cusPhone = new CusPhone;
          $cusPhone->customer_id = $customer->id;
          $cusPhone->phone = $request->input('phone_' . $i);
          $cusPhone->save();
        }
        
      }

      Log::info('Customer Updated: Customer Id: '.$customer->id);
      //return successful response
      return response()->json(['res' => 'Customer updated successfully.!', 'status' => 1]);

    } catch (\Exception $e) {
        //return error message
        Log::info('Customer Registration Error');
        return response()->json(['res' => 'Customer Registration Failed!', 'status' => 0]);
    }
  }

  /**
   * Search customer by First Name, Last Name, Email or Phone Numbers.
   *
   * @param  term  $request
   * @return Response json array
   */
  public function searchCustomers($term)
  {
    try 
    {
      if ($term != 'all')
        $customer = Customer::with('phones')
        ->when($term,  function($query) use ($term) {
          $query->whereHas('phones', function($phones) use ($term) {
            return $phones->where('phone', 'like', '%' . $term . '%');
          });
        })
        ->orWhere('first_name', 'like', '%' . $term . '%' )
        ->orWhere('last_name', 'like', '%' . $term . '%' )
        ->orWhere('email', 'like', '%' . $term . '%' )
        ->orderBy('first_name')->get();
      else
        $customer = Customer::with('phones')->orderBy('first_name')->get();
        
      return response()->json($customer);
    } catch (\Exception $e) {
      Log::info('Search Customer Error: ' . json_encode($e));
      return response()->json(['status' => 0]);
    }
    
  }

  /**
   * Delete customer by its customer id
   *
   * @param  id  $request
   * @return Response json array
   */  
  public function deleteCustomer($id)
  {
    try {
      $customer = Customer::find($id);
      $customer->delete();

      $cusPhone = CusPhone::where(['customer_id' => $id])->get();
      if ($cusPhone)
        foreach($cusPhone AS $phone)
          $phone->delete();
      
      return response()->json(['res' => $cusPhone, 'status' => 1]);
    } catch (\Exception $e) {
      Log::info('Customer Delete Error: ' . json_encode($e));
      return response()->json(['status' => 0, 'data' => json_encode($e)]);
    }
  }

  /**
   * Get Customer details for customer id.
   *
   * @param  id  $request
   * @return Response json array
   */
  public function getCustomerbyId($id)
  {
    $customer = Customer::with('phones')->where('id', $id)->first();
    return response()->json($customer);
  }

  /**
   * Upload CSV to Create or Update customer data as a bulk insert
   *
   * @param  Request  $request
   * @return Response json array
   */
  public function uploadCsv(Request $request)
  {
    try 
    {
      $file = $request->input('file');
      if($request->file()) {
        $fileName = time().'_'.$request->file->getClientOriginalName();

        // Store CSV file in storage
        $uploadFile = $request->file('file')->storeAs('uploads', $fileName, 'public');
        $filePath = Storage::path('public/' . $uploadFile);

        // Open CSV File from Storage for process
        $csvFile = fopen($filePath, 'r');

        // Ignore CSV Header row 
        fgetcsv($csvFile, 1000, ",");
        while (($line = fgetcsv($csvFile)) !== FALSE) {
          $customerId = 0;

          // Checks if the email is empty
          if (!empty($line[5]))
          {
            $customer = Customer::where(['email' => $line[5]])->first();
            if ($customer)
            {
              $customer->first_name = (empty($line[0])) ?: $line[0];
              $customer->last_name = (empty($line[1])) ?: $line[1];
              $customer->address1 = (empty($line[2])) ?: $line[2];
              $customer->address2 = (empty($line[3])) ?: $line[3];
              $customer->city = (empty($line[4])) ?: $line[4];
              $customer->update();
            }
            else
            {
              $customer = new Customer;
              $customer->first_name = (empty($line[0])) ?: $line[0];
              $customer->last_name = (empty($line[1])) ?: $line[1];
              $customer->address1 = (empty($line[2])) ?: $line[2];
              $customer->address2 = (empty($line[3])) ?: $line[3];
              $customer->city = (empty($line[4])) ?: $line[4];
              $customer->email = (empty($line[5])) ?: $line[5];
              $customer->save();
            }
            $customerId = $customer->id;
          }

          // Checks if the Phones colum is empty
          if (!empty($line[6]))
          {
            $phoneArray = explode(',', $line[6]);
            for($i = 0; $i < count($phoneArray); $i++) {
              $cusPhone = CusPhone::where('phone', $phoneArray[$i])->first();
              if (empty($cusPhone))
              {
                $cusPhone = new CusPhone;
                $cusPhone->customer_id = $customerId;
                $cusPhone->phone = $phoneArray[$i];
                $cusPhone->save(); 
              }
            }
          }
        }
        fclose($csvFile);
        Log::info('CSV Upload customer: Customer Id: '.$customerId);
      }
      //return successful response
      return response()->json(['res' => 'CSV Uploaded successfully.!', 'status' => 1]);

    } catch (\Exception $e) {
        //return error message
        Log::info('CSV Upload Error');
        return response()->json(['res' => 'CSV Upload Failed!', 'status' => 0, 'error' => $e->getMessage()]);
    }
  }
}
