<?php

namespace App\Http\Controllers;

use Log;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
   }

   /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
      //validate incoming request 
      $this->validate($request, [
         'email' => 'required|string',
         'password' => 'required|string',
      ]);
      $credentials = $request->only(['email', 'password']);
      if (! $token = Auth::attempt($credentials)) {
         Log::info('User Login Error: Unauthorized');
         return response()->json(['res' => 'Unauthorized', 'status' => 0]);
      }
      $userData = Auth::user();
      return response()->json(['res' => $this->respondWithToken($token)->original, 'user' => $userData, 'status' => 1]);;
   }
    

}
